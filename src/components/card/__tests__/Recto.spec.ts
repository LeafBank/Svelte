import { render, screen } from '@testing-library/svelte';
import Recto from '../Recto.svelte';

describe('<Recto />', () => {
  it('Should Match The Snapshot', () => {
    const { container } = render(Recto, {
      props: {
        cardNumber: '**** 1234 **** 5678',
        fullName: 'JOHN DOE',
        month: '12',
        year: '34'
      }
    });

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', () => {
    render(Recto, {
      props: {
        cardNumber: '**** 1234 **** 5678',
        fullName: 'JOHN DOE',
        month: '12',
        year: '34'
      }
    });

    expect(screen.getByText('**** 1234 **** 5678')).toBeInTheDocument();
    expect(screen.getByText('JOHN DOE')).toBeInTheDocument();
    expect(screen.getByText('12/34')).toBeInTheDocument();
  });
});
