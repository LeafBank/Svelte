import App from './components/App.svelte';
import './index.css';

const app = new App({
  target: document.getElementById('root')
});

export default app;
